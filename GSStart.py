# -*- coding:utf-8 -*-
import json
import socket
import socketserver
import ssl
import traceback

from Config.getKeysAndCerts import getPathToCert, getPathToKey
from Config import values
from DataBaseConnector.Tables import Address
from readSocket import matchLengthMessage, readMessageFromSocket

class ThreadedTCPRequestHandler(socketserver.BaseRequestHandler):
    def handle(self):
        request = readMessageFromSocket(self.request)
        if values.DEBUG_MODE_ENABLED:
            print("From client", self.client_address, request)
        if request == "PING":
            self.request.sendall( matchLengthMessage("PONG") )
        else:
            try:
                requestJSON = json.loads(request)
                requestKeys = requestJSON.keys()
                if len(requestKeys) == 3 and "type" not in requestKeys:
                    if ("City" in requestKeys and "Street" in requestKeys and "House" in requestKeys):
                        # for old api
                        if " " not in requestJSON["House"]:
                            result = json.dumps(Address.getAddressCoordinates(city=requestJSON["City"],
                                                                              street=requestJSON["Street"],
                                                                              house=requestJSON["House"]))
                        else:
                            house = requestJSON["House"].split()
                            result = json.dumps(Address.getAddressCoordinates(city=requestJSON["City"],
                                                                              street=requestJSON["Street"],
                                                                              house=house[0],
                                                                              building=house[1]))
                        self.request.sendall( matchLengthMessage(result) )
                    elif ("city" in requestKeys and "street" in requestKeys and "house" in requestKeys):
                        # for new api
                        if " " not in requestJSON["house"]:
                            result = json.dumps(Address.getAddressCoordinates(city=requestJSON["city"],
                                                                              street=requestJSON["street"],
                                                                              house=requestJSON["house"]))
                        else:
                            house = requestJSON["house"].split()
                            result = json.dumps(Address.getAddressCoordinates(city=requestJSON["city"],
                                                                              street=requestJSON["street"],
                                                                              house=house[0],
                                                                              building=house[1]))
                        self.request.sendall( matchLengthMessage(result) )
                elif requestJSON["type"] == "getAddressCoordinates":
                    params = requestJSON["values"]
                    if ("building" not in params.keys()):
                        result = json.dumps(Address.getAddressCoordinates(city=params["city"],
                                                                          street=params["street"],
                                                                          house=params["house"]))
                    else:
                        result = json.dumps(Address.getAddressCoordinates(city=params["city"],
                                                                          street=params["street"],
                                                                          house=params["house"],
                                                                          building=params["building"]))
                    self.request.sendall(matchLengthMessage(result))
                elif requestJSON["type"] == "checkAddress":
                    pass
                else:
                    self.request.sendall( matchLengthMessage( json.dumps({"success": False, "error": "incorrect request"}) ) )
            except Exception as ex:
                if values.DEBUG_MODE_ENABLED:
                    print(traceback.format_exc())
                self.request.sendall( matchLengthMessage( json.dumps({"success": False, "error": traceback.format_exc()}) ) )

class ThreadedTCPServer(socketserver.ThreadingMixIn, socketserver.TCPServer):
    def __init__(self, myaddress, handler, bind_and_activate=True):
        socketserver.TCPServer.__init__(self, myaddress, handler)
        self.logRequests = True

        self.socket = ssl.wrap_socket(socket.socket(self.address_family, self.socket_type),
                                      server_side=True,
                                      certfile=getPathToCert(values.cert),
                                      keyfile=getPathToKey(values.key),
                                      ca_certs=getPathToCert(values.cert),
                                      cert_reqs=ssl.CERT_REQUIRED,
                                      ssl_version=ssl.PROTOCOL_TLSv1_2)

        if bind_and_activate:
            self.server_bind()
            self.server_activate()

    def shutdown_request(self, request):
        request.shutdown(socket.SHUT_WR)


if __name__ == "__main__":
    server = ThreadedTCPServer((values.host, int(values.port)), ThreadedTCPRequestHandler)
    server.serve_forever()