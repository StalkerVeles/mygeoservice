# -*- coding:utf-8 -*-

import socket
from OpenSSL import SSL
from Config.getKeysAndCerts import getPathToCert, getPathToKey
from Config import values

def sendMessage(host="localhost", port=11000, message="PING", expected_response="simple"):
    s = socket.socket()

    ctx = SSL.Context(SSL.TLSv1_2_METHOD)
    ctx.use_privatekey_file(getPathToKey(values.key))
    ctx.use_certificate_file(getPathToCert(values.cert))

    connection = SSL.Connection(ctx, s)
    connection.connect((host, port))

    query = matchLengthMessage(message)

    connection.send(query)
    response = None
    if expected_response == "simple":
        response = readMessageFromSocket(connection)
    elif expected_response == "sequence":
        response = []
        while 1:
            resp_message = readMessageFromSocket(connection)
            if resp_message != "01:EOF":
                if resp_message[0:2] == "01":
                    response.append(resp_message[3:])
            else:
                break
    connection.close()
    return response

def readMessageFromSocket(connection):
    lenMessage = ""
    data_in = connection.recv(1)
    while (data_in.decode() != ":"):
        lenMessage += data_in.decode()
        data_in = connection.recv(1)

    # for php
    if "/?" in lenMessage:
        lenMessage = lenMessage[lenMessage.index("/?")+2 : ]

    data_in += connection.recv(int(lenMessage))
    data_in = data_in.decode()
    return data_in[1:]

def matchLengthMessage(message):
    lm = str(len(message.encode()))
    lm = lm + ":" + message
    return lm.encode()

if __name__ == "__main__":
    sendMessage()
