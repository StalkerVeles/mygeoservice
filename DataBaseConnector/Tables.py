# -*- coding:utf-8 -*-

import traceback

import sys, os
sys.path.append(os.path.join(os.path.dirname(__file__), ".."))

from sqlalchemy import Column
from sqlalchemy.dialects.postgresql import TEXT, INTEGER

from geocoders import geocodeAddress
from Config.values import DEBUG_MODE_ENABLED
from DataBaseConnector.DatabaseCreator import Base, Session, engine

class Address(Base):
    __tablename__ = "address"
    id = Column("id", INTEGER, autoincrement=True, primary_key=True)
    country = Column("country", TEXT)
    city = Column("city", TEXT)
    street = Column("street", TEXT)
    house = Column("house", TEXT)
    building = Column("building", TEXT)
    lat = Column("lat", TEXT)
    lon = Column("lon", TEXT)

    def __init__(self, country="россия", city="курган", street="", house="", building="", lat="", lon=""):
        self.country = country
        self.city = city
        self.street = street
        self.house = house
        self.building = building
        self.lat = lat
        self.lon = lon

    @classmethod
    def _recordNewAddress(cls, country="россия", city="курган", street="", house="", building="", lat="", lon=""):
        session = Session()
        obj = cls(country=country, city=city, street=street, house=house, building=building, lat=lat, lon=lon)
        try:
            session.add(obj)
            session.commit()
            session.close()
            return {'success': True}
        except:
            session.rollback()
            session.close()
            return {'success': False, 'error': traceback.format_exc()}

    @classmethod
    def getAddressCoordinates(cls, country="россия", city="курган", street="", house="", building=""):
        country = country.lower()
        city = city.lower()
        street = street.lower()
        house = house.lower()
        building = building.lower()

        session = Session()
        try:
            if street != "" and house != "":
                query = session.query(cls).filter_by(country=country,
                                                     city=city,
                                                     street=street,
                                                     house=house,
                                                     building=building).order_by(cls.building).first()
                if query != None:
                    session.close()
                    return {'success': True, 'result': {'lat': query.lat, 'lon': query.lon}}
                else:
                    result = geocodeAddress.geocode({"city": city, "street": street, "house": house + " " + building})
                    if result['success']:
                        res = Address._recordNewAddress(country=country, city=city, street=street, house=house, building=building, lat=result['result']['lat'], lon=result['result']['lon'])
                        if res['success']:
                            session.commit()
                            session.close()
                            return result
                        else:
                            session.rollback()
                            session.close()
                            return res
                    else:
                        session.rollback()
                        session.close()
                        return result
            else:
                session.close()
                return {'success': False, 'error': 'too few parameters'}
        except:
            session.rollback()
            session.close()
            if DEBUG_MODE_ENABLED:
                print(traceback.format_exc())
            return {'success': False, 'error': traceback.format_exc()}

    @classmethod
    def checkAddress(cls, **params):
        session = Session()
        query_params = ""
        for param, value in params.items():
            query_params += " {0}='{1}' AND".format(param, value)
        query_params = query_params[0: query_params.rfind(" AND")]
        try:
            query = session.execute('SELECT * FROM "address" WHERE{0}'.format(query_params))
            res = []
            for i in query:
                res.append({'country': i[1], 'city': i[2], 'street': i[3], 'house': i[4], 'building': i[5]})

            session.close()
            return {'success': True, 'result': res}
        except:
            session.close()
            if DEBUG_MODE_ENABLED:
                print(traceback.format_exc())
            return {'success': False, 'error': traceback.format_exc()}


    def __repr__(self):
        return "<Address(address='{country}, {city}, {street} {house} {building}', lat={lat}, lon={lon})>".format(country=self.country, city=self.city,
                                                                                                                  street=self.street, house=self.house,
                                                                                                                  building=self.building, lat=self.lat,
                                                                                                                  lon=self.lon)

if __name__ == "__main__":
    Base.metadata.create_all(engine)
    #Address.recordNewAddress(street="калинина", house="18", building="А", lat="55.448260", lon="65.317738")
    #Address.recordNewAddress(street="калинина", house="18", building="Б", lat="55.448260", lon="65.317738")
    #Address.recordNewAddress(street="бурова-петрова", house="119", lat="55.480182", lon="65.332416")
    #print(Address.getAddressCoordinates(street="калинина", house="18", building="А"))
    #print(Address.getAddressCoordinates(street="5 Микрорайон", house="33"))