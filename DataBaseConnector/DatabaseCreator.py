# -*- coding:utf-8 -*-

from sqlalchemy import create_engine
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker

from Config.values import DEBUG_MODE_ENABLED, DB_HOST, DB_NAME, DB_PASS, DB_USER

engine = create_engine("postgresql://{user}:{password}@{host}/{db_name}".format(user=DB_USER,
                                                                                password=DB_PASS,
                                                                                host=DB_HOST,
                                                                                db_name=DB_NAME),
                       encoding="utf8",
                       echo=DEBUG_MODE_ENABLED,
                       isolation_level="READ UNCOMMITTED")
Session = sessionmaker(engine, autoflush=True)
Base = declarative_base()