# -*- coding:utf-8 -*-
from geocoders import Dgis

from geocoders import YandexGeocoder

def geocode(address):
    errors = []
    response = YandexGeocoder.getCoordinates(address)
    if response["error"] == "":
        response.pop("error")
        return {"success": True, "result": response}
    else:
        errors.append("yandex: %s" % str(response["error"]))

    response = Dgis.getCoordinates(address)
    if response["error"] == "":
        response.pop("error")

        return {"success": True, "result": response}
    else:
        errors.append("dgis: %s" % str(response["error"]))
        return {"success": False, "error": str(errors)}

if __name__ == "__main__":
    # {'success': True, 'result': {'lat': '55.449021', 'lon': '65.321122'}} yandex
    # {'success': True, 'result': {'lat': '55.44904', 'lon': '65.321192'}} dgis
    #print(geocode({"city": "Курган", "street": "Калинина", "house": ""}))
    #print(geocode({"city": "Курган", "street": "Калинина", "house": "16"}))
    #print(geocode({"city": "Курган", "street": "Хмельницкого", "house": "22"}))
    #print(geocode({"city": "Курган", "street": "Богдана Хмельницкого", "house": "22"}))
    print(geocode({'city': 'Курган', 'street': 'зелёная', 'house': '8'}))