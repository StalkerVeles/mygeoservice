# -*- coding:utf-8 -*-
from grab import Grab
import json

def loadPage(g=None, url=""):
    result = {}
    for i in range(3): # i know, it's a bad practice
        try:
            g.go(url)
            result = {'error':'', 'body': g.doc.body.decode()}
            break
        except Exception as ex:
            result = {'error': 'Can`t load page. %s' % str(ex)}
    return result

def getCoordinates(address={'city':'', 'street':'', 'house':''}):
    g = Grab()
    url = 'https://geocode-maps.yandex.ru/1.x/?format=json&results=1&lang=ru_RU&geocode={city},+{street},+{house}'.format(city=address['city'],
                                                                                                                          street=address['street'],
                                                                                                                          house=address['house'])

    page = loadPage(g=g, url=url)
    if page['error'] == '':
        try:
            response = json.loads(page['body'])
            geoobject = response["response"]["GeoObjectCollection"]["featureMember"][0]["GeoObject"]
            if geoobject["metaDataProperty"]["GeocoderMetaData"]["precision"] == "exact":
                if geoobject["metaDataProperty"]["GeocoderMetaData"]["kind"] == "house":
                    Point = geoobject["Point"]["pos"].split(" ")
                    return {'error': '', 'lon': Point[0], 'lat': Point[1]}
                else:
                    return {'error': 'Not find type "house"'}
            else:
                return {'error': 'Result is not exact'}
        except Exception as ex:
            return {'error': 'Some error: %s' % str(ex)}
    else:
        return page

if __name__ == "__main__":
    #print(getCoordinates({'city':'', 'street':'', 'house':''}))
    print(getCoordinates({'city': 'Курган', 'street': 'молодёжи', 'house': '4'}))
    #print(getCoordinates({'city':'Курган', 'street':'Богдана Хмельницкого', 'house':'22'}))
    #print(getCoordinates({'city':'Курган', 'street':'Хмельницкого', 'house':'22'}))
    #print(getCoordinates({'city':'Курган', 'street':'зелёная', 'house':'8'}))