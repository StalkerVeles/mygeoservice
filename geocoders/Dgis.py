# -* coding:utf-8 -*-

from grab import Grab

from geocoders.YandexGeocoder import loadPage
from CustomSocketReader import sendMessage
from Config.values import SERVICE_ZONE_HOST, SERVICE_ZONE_PORT


def extractApiKeyFromResponse(response=""):
    if response.count("webApiKey") > 0:
        response = response[response.find('webApiKey":"') + len('webApiKey":"'):]
        response = response[0: response.find('"')]
        return response
    else:
        return ""

def extractCoordinatesFromResponse(response=""):
    try:
        import json
        responseJSON = json.loads(response)
        if responseJSON["result"]["total"] == 1:
            lat = responseJSON["result"]["items"][0]["lat"]
            lon = responseJSON["result"]["items"][0]["lon"]
            checkServiceForAddress = json.loads(sendMessage(host=SERVICE_ZONE_HOST,
                                                            port=int(SERVICE_ZONE_PORT),
                                                            message=json.dumps({'type': 'IdentifyServiceStoreByCoordinates',
                                                                                'coordinates': {'lat': lat, 'lon': lon}})))
            if checkServiceForAddress['result'] != []:
                return {'error': '', 'lat': lat, 'lon': lon}
            else:
                return {'error': 'Result is empty'}
        elif responseJSON["result"]["total"] == 0:
            return {'error': 'Result is empty'}
        elif responseJSON["result"]["total"] > 1:
            result = {'error': 'Result is empty'}
            for item in responseJSON["result"]["items"]:
                lat = item["lat"]
                lon = item["lon"]
                checkServiceForAddress = json.loads(sendMessage(host=SERVICE_ZONE_HOST,
                                                                port=int(SERVICE_ZONE_PORT),
                                                                message=json.dumps({'type': 'IdentifyServiceStoreByCoordinates',
                                                                                    'coordinates': {'lat': lat, 'lon': lon}})))
                if checkServiceForAddress['result'] != []:
                    result = {'error': '', 'lat': lat, 'lon': lon}
                    break
            return result
    except:
        return {'error': 'Can`t extract coordinates from "%s"' % response}

def getCoordinates(datas={'city': '', 'street': '', 'house': ''}):
    try:
        url = "https://2gis.ru/%s" % datas['city']

        g = Grab()
        try:
            g.go(url)
        except:
            return {'error': 'Can`t load page'}

        key = ""
        # i know, it's a bad practice
        while 1:
            g = Grab()

            page = loadPage(g=g, url=url)
            if page["error"] == "":
                key = extractApiKeyFromResponse(page["body"])
                if key != "":
                    break

        if key == "":
            return {'error': 'Key not found'}
        else:
            url = "https://catalog.api.2gis.ru/3.0/markers?page=1&page_size=10000&q={street} {house}&region_id=10&locale=ru_RU&key={key}".format(street=datas['street'],
                                                                                                                                                                         house=datas['house'],
                                                                                                                                                                         key=key)

            page = loadPage(g=g, url=url)
            if page['error'] == '':
                return extractCoordinatesFromResponse(page['body'])
            else:
                return {'error': page['error']}
    except Exception as ex:
        return {'error': 'No result, error: "%s"' % str(ex)}

if __name__ == "__main__":
    # {"result": {"lat": "55.46151", "lon": "65.293724"}, "success": true}
    #print( getCoordinates({'city': 'Курган', 'street': 'Калинина', 'house': '16'}) ) # True
    #print( getCoordinates({'city': 'Курган', 'street': 'Хмельницкого', 'house': '22'}) ) # True
    #print( getCoordinates({'city': 'Курган', 'street': 'Молодёжи переулок', 'house': '46С'}) ) # False
    print( getCoordinates({'city': 'Курган', 'street': 'карбышева', 'house': '35 4'}) ) # True

    #{"id":"1408010538741397","lon":"65.373744","lat":"55.509308","vital":1}, Большое Чаусово
    # {"id":"1408010538729558","lon":"65.293724","lat":"55.46151","vital":1}, True
    # {"id":"1408010538727494","lon":"65.310547","lat":"55.447599","vital":1} Северный