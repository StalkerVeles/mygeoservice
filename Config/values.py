# [hosts]
#host = "0.0.0.0"
host = "localhost"
port = "12000"
httphost = "0.0.0.0"
httpport = 12043
SERVICE_ZONE_HOST = "x.x.x.x"
SERVICE_ZONE_PORT = "12730"

# [certificates]
cert = "certs/server2.pem"
key = "keys/server2.key"

# [debugs]
DEBUG_MODE_ENABLED = True

# [databases]
DB_USER = "autorouter_user"
DB_PASS = "autoroute"
DB_HOST = "localhost"
DB_NAME = "geocoder_db"