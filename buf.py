# -*- coding:utf-8 -*-

from http.client import HTTPSConnection
import ssl

ctx = ssl.SSLContext(ssl.PROTOCOL_TLS)
httpsc = HTTPSConnection('localhost', port=12043, context=ctx)

httpsc.request("POST", 'localhost:12043', b"PING")
print(httpsc.getresponse().read())