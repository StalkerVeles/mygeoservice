# -*- coding:utf-8 -*-

def readMessageFromSocket(connection):
    lenMessage = ""
    data_in = connection.recv(1)
    while (data_in.decode() != ":"):
        lenMessage += data_in.decode()
        data_in = connection.recv(1)

    data_in += connection.recv(int(lenMessage))
    data_in = data_in.decode()
    return data_in[1:]

def matchLengthMessage(message):
    lm = str(len(message))
    lm = lm + ":" + message
    return lm.encode()