# -*- coding:utf-8 -*-

from http.server import HTTPServer, BaseHTTPRequestHandler
import ssl, urllib, json
from Config.getKeysAndCerts import getPathToCert, getPathToKey
from Config import values
import traceback

from DataBaseConnector.Tables import Address

class MyHandler(BaseHTTPRequestHandler):
    def _set_headers(self):
        self.send_response(200)
        self.send_header('Content-type', 'application/json')
        self.send_header('Access-Control-Allow-Origin', '*')
        self.end_headers()

    def do_GET(self):
        self._set_headers()
        self.wfile.write( bytes(json.dumps({'success': False, 'error': 'I need POST, Lebovski. Where is my POST?'}), encoding="utf-8") )

    def do_HEAD(self):
        self._set_headers()

    def do_POST(self):
        content_length = int(self.headers['Content-Length'])
        request = urllib.parse.unquote(self.rfile.read(content_length).decode())

        if request == "PING":
            self._set_headers()
            self.wfile.write( b"PONG" )
        else:
            try:
                requestJSON = json.loads(request)
                requestKeys = requestJSON.keys()
                if "type" in requestKeys:
                    if requestJSON["type"] == "getAddressCoordinates":
                        params = requestJSON["values"]
                        if ("building" not in params.keys()):
                            result = json.dumps(Address.getAddressCoordinates(city=params["city"],
                                                                              street=params["street"],
                                                                              house=params["house"]))
                        else:
                            result = json.dumps(Address.getAddressCoordinates(city=params["city"],
                                                                              street=params["street"],
                                                                              house=params["house"],
                                                                              building=params["building"]))
                        self._set_headers()
                        self.wfile.write( bytes(result, encoding="utf-8") )
                    elif requestJSON["type"] == "checkAddress":
                        pass
                    else:
                        self._set_headers()
                        self.wfile.write( bytes( json.dumps({"success": False, "error": "incorrect request"}), encoding="utf-8" ) )
                else:
                    self._set_headers()
                    self.wfile.write( bytes(json.dumps({"success": False, "error": "unknown type response"}), encoding="utf-8") )
            except:
                if values.DEBUG_MODE_ENABLED:
                    print(traceback.format_exc())
                self._set_headers()
                self.wfile.write( bytes( json.dumps({"success": False, "error": "unknown error"}), encoding="utf-8" ) )

httpd = HTTPServer((values.httphost, values.httpport), MyHandler)
#httpd.socket = ssl.wrap_socket(httpd.socket,
#                               certfile=getPathToCert(values.cert),
#                               server_side=True,
#                               keyfile=getPathToKey(values.key))
httpd.serve_forever()